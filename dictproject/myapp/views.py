from django.shortcuts import render
import requests

def dict(request):
    dictword = {}
    if 'word' in request.GET:
        word = request.GET['word']
        url='https://api.dictionaryapi.dev/api/v2/entries/en/%s' % word
        response = requests.get(url)
        dictword = response.json()
    return render(request,"dict.html",{'dictword':dictword})

 